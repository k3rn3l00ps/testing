const express = require("express");
const path = require("path");

const complements = {
     "You look nice today",
     "That dress looks nice on you",
     "Have you been working out?",
     "You can do hard things",
     "You've gotten far in this course. You're really smart",
     "You're programming! How cool is that?",
     "I'm really proud of you",
     "You made this",
     "You've learned a lot of things, and that's pretty hard to do"
}

function getRandomComplement() {
     const randomIndex = Math.floor(math.random() * complements.length);
     return complements[randomIndex];
}

const app = express();

app.get("/", function(req, res) {
  res.sendFile(path.join(_dirname, "index.html")); // send index.html when client browser requests http
});

app.get("/complement", function(req, res) {
     res
          .json({
               complement: getRandomComplement()
          })
          .end();
});

app.use("/public", express.static("./public"));

app.listen(3000);
console.log("listening on http://localhost:3000");

// favicon.ico is used as tab icon

// node.js - executes JavaScript outside of a browser
// npm - node package manager (installs node packages)

// go to project within shell
     // npm install --global parcel-bundler
          // installs parcel which is used to bundle JavaScript
     // npm init -y
          // creates node project out of project
     // npm install express
          // simple server framework (most popular)
     // parcel index.html
          // reads html file and bundles css and js files
     // node server.js
          // runs javascript file as a server
