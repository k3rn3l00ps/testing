document
     .querySelector(".request-complement")
     .addEventListener("click", function() {
          fetch("/complement")
               .then(function(res) {
                    return res.json();
               })
               .then(function(data) {
                    document.querySelector(".complement").innerText = data.complement;
               })
               .catch(err => {
                    console.error(err);
               });
     });
